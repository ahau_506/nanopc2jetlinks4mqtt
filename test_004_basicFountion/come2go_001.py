from unittest import result
import serial
import time

import ctypes
# ll = ctypes.cdll.loadLibrary   
lib = ctypes.cdll.LoadLibrary("./duoji")
# lib = ll("./duoji")
moveAngle_1=90
moveAngle_2=90

# 打开串口COM4，进行通信，四合一传感器,光照、湿度、二氧化碳、温度
ser = serial.Serial("/dev/ttyUSB1", 9600)
# 打开串口COM5，进行通信,氨气参数
serN = serial.Serial("/dev/ttyUSB0", 9600)
i=1

# python 3.6





import random
import time
import hashlib
from paho.mqtt import client as mqtt_client
import json
import time
 
t = time.time()

def str2md5(str):
    '''使用MD5对字符串进行加密

    Args:
        str (str): 需要加密的字符串

    Returns:
        [str]: 32位字符串
    ''' 
    m = hashlib.md5()  # 创建md5对象
    
    str_en = str.encode(encoding='utf-8')  # str必须先encode
    m.update(str_en)  # 传入字符串并加密
    str_md5 = m.hexdigest()  # 将MD5 hash值转换为16进制数字字符串
    return str_md5

 
# 13位 时间戳 (毫秒)
int(round(t * 1000))

secureId= 'ahau'
secureKey='ahau'
now = int(round(t * 1000)); #//当前时间戳
username_1 = secureId+"|"+str(now); #// 拼接用户密码
password_1 = str2md5(username_1+"|"+secureKey); #//使用md5生成摘要

print("username_1="+username_1)
print("password_1="+password_1)

# broker = 'iot.gitnote.cn'
broker = '42.192.123.52'
port = 1883
# https://lanp.nioee.com 内网穿透服务
# broker = '139.155.180.150'
# port = 10070
topic = "/AHHFSS_001/AH_HF_SH_001_001/properties/report"
# generate client ID with pub prefix randomly
client_id = "AH_HF_SH_001_001"


def connect_mqtt():
    def on_connect(client, userdata, flags, rc):
        if rc == 0:
            print("Connected to MQTT Broker!")
        else:
            print("Failed to connect, return code %d\n", rc)

    
    client = mqtt_client.Client(client_id)
    # client.username_pw_set(username_1,password_1)
    client.username_pw_set(username_1,password_1)
    client.on_connect = on_connect
    client.connect(broker, port,60)
    return client

# wendu=33.3
# shidu=50
# guangzhao=300
# co2=150
# NH3=14


def getData():
    if ser.is_open and serN.is_open:
        # 四合一传感器的控制命令
        send_data = bytes.fromhex('01 03 00 00 00 04 44 09')
        # 氨气传感器的控制命令
        send_dataN = bytes.fromhex('01 03 00 00 00 01 84 0A')
        # 向四合一传感器发送命令
        ser.write(send_data)
        # 向氨气传感器发送命令
        serN.write(send_dataN)
        # sleep中的单位为秒,传感器在300ms内响应
        time.sleep(1)
        # 四合一传感器读到的长度
        len_return_data = ser.inWaiting()
        print(len_return_data)
        # 氨气传感器读到的长度
        len_return_dataN = serN.inWaiting()
        print(len_return_dataN)
        if len_return_data and len_return_dataN:
            # 四合一传感器读到的数据
            return_data = ser.read(len_return_data)
            # 氨气传感器读到的数据
            return_dataN = serN.read(len_return_dataN)
            # 四合一传感器转为十六进制
            str_return_data = str(return_data.hex())
            # 氨气传感器转为十六进制
            str_return_dataN = str(return_dataN.hex())

            serData={}
            # 数据转化部分
            # CO2data
            global i
            print(i)
            i+=1
            CO2data = (int(str_return_data[6:8], 16)) * 256 + int(str_return_data[8:10], 16)
            serData['co2']=CO2data
            # print('二氧化碳：%s ppm' % CO2data)
            # a1.value=CO2data
            #a1[0]=CO2data
            # wendata
            if int(str_return_data[10:12], 16) < 127:
                wendata = (int(str_return_data[10:12], 16) * 256 + int(str_return_data[12:14], 16)) / 100
                # print('温度：%s 度' % wendata)
            else:
                wendata = (((int(str_return_data[10:12], 16) & 0x7F)) * 256 + int(str_return_data[12:14], 16)) / 100
                # print('温度：%s' % wendata)
            serData['wendu']=wendata
            #a1[1]=wendata
            # a2.value=wendata
            # 湿度
            shidudata = ((int(str_return_data[14:16], 16)) * 256 + int(str_return_data[16:18], 16)) / 100
            # print('湿度：%s' % shidudata + '%RH')
            serData['shidu']=shidudata
            # a3.value=shidudata
            # 光照
            guangzhaodata = ((int(str_return_data[18:20], 16)) * 256 + int(str_return_data[20:22], 16)) * 4
            # print('光照：%s lx' % guangzhaodata)
            serData['guangzhao']=guangzhaodata
            # a4.value=guangzhaodata
            # 氨气
            NH3data = int(str_return_dataN[6:8], 16) + int(str_return_dataN[8:10], 16)
            # print('氨气：%s ppm' % NH3data)
            serData['NH3']=NH3data
            # a5.value=NH3data
            return serData


# msg=str({"properties": {"wendu": wendu,"shidu":shidu,"guangzhao":guangzhao,"co2":co2,"NH3":NH3},"deviceId": "ZN_001_001","success": 'true'})
msg=str({"properties": getData()});

def publish(client):
        msg_count = 0
    # while True:
        # time.sleep(3)
        result = client.publish(topic, str({"properties": getData()}))
        # bbb=client.subscribe('/ZN_001/ZN_001_001/function/invoke')
        # print("bbb=",bbb)
        # global wendu
        # global shidu
        # wendu+=1.0
        # shidu+=1
        # print("wendu=",wendu,"shidu=",shidu)
        print(msg)
        # result: [0, 1]
        status = result[0]
        if status == 0:
            print(f"Send `{msg}` to topic `{topic}`")
        else:
            print(f"Failed to send message to topic {topic}")
        msg_count += 1
        
def subscribe(client):
    # while True:
        # time.sleep(2)
        result=client.subscribe('/AHHFSS_001/AH_HF_SH_001_001/function/invoke',1)
        print("result=",result)

#  b前缀字符串是 bytes字符串，ascll码字符串
def on_message(client, userdata, msg):
    ss=""
    print(msg.topic+" " + ":" + str(msg.payload))
#     bytes转字符串str
    ss= bytes.decode(msg.payload)
    print("run_go=",type(ss))
    print("run_go=",type(json.loads(ss))) # dict
    print("run_go=",json.loads(ss)["function"]) # 字典格式取值
    print("run_go=",type(json.loads(ss)["function"])) # 字典格式取值

    duojiGo=json.loads(ss)["function"]
    if(duojiGo=="shang" or duojiGo=="xia" or duojiGo=="zuo" or duojiGo=="you"):
        global moveAngle_1
        global moveAngle_2
        if(duojiGo=="shang"):
            print("goShang")
            moveAngle_1=lib.fmain(0,moveAngle_1)
        elif(duojiGo=="xia"):
            print("goxia")
            moveAngle_1=lib.fmain(1,moveAngle_1)
        elif(duojiGo=="zuo"):
            print("gozuo")
            moveAngle_2=lib.fmain(2,moveAngle_2)
        elif(duojiGo=="you"):
            print("goyou")
            moveAngle_2=lib.fmain(3,moveAngle_2)


def run():
    client = connect_mqtt()
    client.on_message = on_message
    # subscribe(client)
    client.loop_start()
    subscribe(client)
    while True:
        time.sleep(3)
        publish(client)
        time.sleep(2)





if __name__ == '__main__':
    run()
