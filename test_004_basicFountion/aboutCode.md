## nanopc 通过 mqtt 协议与 jetlinks 平台互通基础功能 
本目录代码实现了一个从 ARM_Linux 开发板采集传感器数据与视频数据并实时传输到 jetlinks 平台，并能实时从 jetlinks 平台获取控制信息的功能。

其中 `come2go.py` 实现了本地开发板通过 `mqtt` 协议实时发送传感器采集到的数据到 `jetlinks` 物联网平台，同时实时获取 `jetlinks` 下发的控制信息，根据控制信息调用 `duoji.c` 实时控制舵机云台的转动。

`rtmp.py` 实现了通过 `opencv` 实时采集摄像头的数据并使用 `rtmp` 协议通过自建 `nginx-rtmp` 服务器或腾讯云直播 将视频数据传输到 `jetlinks` 物联网平台中。



`jetlinks` 物联网平台部署在实验室工作站中，内网ip为：`172.17.14.191` ，展示页面端口号为：9000，mqtt服务器端口号为：1883。
`jetlinks` 展示页面内网访问地址为：`172.17.14.191:9000`,通过natfrp内网穿透服务暴露到公网中，公网地址：`iot2.gitnote.cn` 。
`jetlinks mqtt` 服务器内网地址为：`172.17.14.191:1883`，通过 `https://lanp.nioee.com` 内网穿透服务暴露到公网中，公网访问地址为：`139.155.180.XXX:10070`。


nginx-rtmp 服务器使用 docker 部署在腾讯云（1.15.120.XXX）服务器中，并能实时保存视频文件到服务器中（修改文件支持），其访问地址为 `iot.gitnote.cn:1935` 或 `1.15.120.XXX:1935`.
nginx-rtmp 服务器试试保存的视频通过 `Zdir` 搭建的目录展示程序通过 `video.gitnote.cn` 访问获取。

具体代码在 https://gitee.com/qiaoyukeji/nanopc2jetlinks4mqtt/tree/master/test_004_basicFountion
