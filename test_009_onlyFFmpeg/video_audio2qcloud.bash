# 直接使用 ffmpeg 采集音视频数据传输到腾讯云平台，未做任何处理，延时小于 1s
ffmpeg -f alsa -thread_queue_size 4096 -ac 2 -i hw:4,0 -r 48000 -f v4l2 -thread_queue_size 4096 -r 30 -i /dev/video10 -c:v libx264 -s 640x480 -pix_fmt yuv420p -preset ultrafast  -c:a aac -ar 44100  -f flv rtmp://156756.livepush.myqcloud.com/live/test &> ffmpeg_log.log


# 仅视频
ffmpeg  -f v4l2 -thread_queue_size 4096 -r 30 -i /dev/video10 -c:v libx264 -s 640x480 -pix_fmt yuv420p -preset ultrafast    -f flv rtmp://156756.livepush.myqcloud.com/live/test &> ffmpeg_log.log