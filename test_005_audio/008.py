#录制音频
import wave
import pyaudio

CHUNK = 1024
FORMAT = pyaudio.paInt16
CHANNELS = 1
# RATE = 44100
RATE = 48000
#RATE = 22050
RECORD_SECONDS =30
p = pyaudio.PyAudio()
#i=0
while True:
    #i=i+1
    # 数据流
    stream = p.open(format=FORMAT, channels=CHANNELS, rate=RATE, input=True,input_device_index=3, frames_per_buffer=CHUNK)
    print("开始录音!")
    frames = []
    for i in range(0, int(RATE / CHUNK * RECORD_SECONDS)):
        data = stream.read(CHUNK,exception_on_overflow = False)
        frames.append(data)
    print("录音完毕！")
    # 停止数据流
    stream.stop_stream()
    stream.close()
    # 关闭PyAudio
    #p.terminate()#不能这样操作，否则会出现不能循环的现
    # 写入录音文件
    wf = wave.open('011.wav', 'wb')
    wf.setnchannels(CHANNELS)
    wf.setsampwidth(p.get_sample_size(FORMAT))
    wf.setframerate(RATE)
    wf.writeframes(b''.join(frames))
    wf.close()
    print('音频数据写入完成')
    break;