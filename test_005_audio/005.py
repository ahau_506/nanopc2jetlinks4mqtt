FFMPEG_BIN = "ffmpeg" # on Linux
#FFMPEG_BIN = "ffmpeg.exe" # on Windows
import pyaudio
import wave
import sys
import subprocess as sp
import time
import numpy
 
command = [ FFMPEG_BIN,
        '-i', 'rtmp://iot.gitnote.cn/live/test',
        '-acodec', 'pcm_s32le',
        '-f', 's16le',
        '-ar', '44100', # ouput will have 44100 Hz
        '-ac', '2', # stereo (set to '1' for mono)
        '-']
pipe = sp.Popen(command, stdout=sp.PIPE, bufsize=10**8)
 
# instantiate PyAudio (1)
p = pyaudio.PyAudio()
 
# define callback (2)
def callback(in_data, frame_count, time_info, status):
    data = pipe.stdout.read(10000)
    data = numpy.frombuffer(data, dtype="int32")
    data = data.reshape((len(data)//2,2))
    #data = pipe.readbuffer(frame_count)
    return (data, pyaudio.paContinue)
 
# open stream using callback (3)
stream = p.open(format=pyaudio.paInt32,
                channels=2,
                rate=44100,
                output=True,
                stream_callback=callback)
 
# start the stream (4)
stream.start_stream()
 
# wait for stream to finish (5)
while stream.is_active():
    time.sleep(0.1)
 
# stop stream (6)
stream.stop_stream()
stream.close()
 
 
# close PyAudio (7)
p.terminate()